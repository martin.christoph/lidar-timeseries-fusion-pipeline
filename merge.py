import argparse
import open3d as o3d
import numpy as np
import os

def join_pcd_files_in_folders(folder1_path, folder2_path, output_folder):

    # Ensure the output folder exists
    os.makedirs(output_folder, exist_ok=True)

    # List all PCD files in the folders
    files1 = [f for f in os.listdir(folder1_path) if f.endswith(".pcd")]
    files2 = [f for f in os.listdir(folder2_path) if f.endswith(".pcd")]

    # Iterate through files with the same name and join them
    for file1 in files1:
        file2 = next((f for f in files2 if f[-10:] == file1[-10:]), None)
        if file2:
            file1_path = os.path.join(folder1_path, file1)
            file2_path = os.path.join(folder2_path, file2)
            output_path = os.path.join(output_folder, file1)
            join_pcd_files(file1_path, file2_path, output_path)
        
def join_pcd_files(file1_path, file2_path, output_path):
    # Read PCD files
    pcd1 = o3d.io.read_point_cloud(file1_path)
    pcd2 = o3d.io.read_point_cloud(file2_path)

    # Convert Open3D point clouds to NumPy arrays
    points1 = np.asarray(pcd1.points)
    points2 = np.asarray(pcd2.points)

    # Concatenate the points from both files
    joined_points = np.concatenate((points1, points2), axis=0)

    # Create a new Open3D point cloud from the joined points
    joined_pcd = o3d.geometry.PointCloud()
    joined_pcd.points = o3d.utility.Vector3dVector(joined_points)
    
    # Write the joined PCD file
    o3d.io.write_point_cloud(output_path, joined_pcd)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Join lidar data from 2 folders")
    parser.add_argument("first_folder", help="Path to first folder")
    parser.add_argument("second_folder", help="Path to second folder")
    parser.add_argument("output_folder", help="Path to output folder")

    args = parser.parse_args()

    join_pcd_files_in_folders(args.first_folder, args.second_folder, args.output_folder)
