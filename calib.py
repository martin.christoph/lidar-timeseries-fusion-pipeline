import os
import open3d as o3d
import numpy as np
import argparse

def apply_yaw_rotation_and_save(root_folder, yaw_angle):
    """
    Apply yaw rotation to all PCD files in the input folder,
    save the rotated point clouds to the output folder with the same filenames.

    Parameters:
    - input_folder: Path to the folder containing input PCD files
    - output_folder: Path to the folder where rotated PCD files will be saved
    - yaw_angle: Yaw angle in radians
    """

    input_folder = os.path.join(root_folder, '2358')
    output_folder = os.path.join(root_folder, '2358_calib')

    # Ensure the output folder exists
    os.makedirs(output_folder, exist_ok=True)

    # Iterate over each file in the input folder
    for filename in os.listdir(input_folder):
        if filename.endswith(".pcd"):
            # Form full paths for input and output files
            input_file_path = os.path.join(input_folder, filename)
            output_file_path = os.path.join(output_folder, filename)

            # Read the original PCD file
            point_cloud = o3d.io.read_point_cloud(input_file_path)

            # Get the XYZ coordinates of the points
            points = np.asarray(point_cloud.points)

            # Create a 3D rotation matrix for yaw
            rotation_matrix = np.array([[np.cos(yaw_angle), -np.sin(yaw_angle), 0],
                                        [np.sin(yaw_angle), np.cos(yaw_angle), 0],
                                        [0, 0, 1]])

            # Apply the rotation matrix to all points in-place
            np.dot(points, rotation_matrix.T, out=points)

            point_cloud.points = o3d.utility.Vector3dVector(points)

            # Save the modified point cloud to the output folder
            o3d.io.write_point_cloud(output_file_path, point_cloud)
            

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Apply yaw rotation to PCD files in the input folder and save them to the output folder.")
    parser.add_argument("root_folder", help="Path to the folder containing input PCD files (with Folder '2358' existing).")
    args = parser.parse_args()

    # Yaw angle in radians (3.35 degrees)
    yaw_angle = np.radians(3.35)

    apply_yaw_rotation_and_save(args.root_folder, yaw_angle)
