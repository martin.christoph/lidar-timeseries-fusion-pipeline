import pandas as pd
import ruptures as rpt
import json
import os
import argparse
import numpy as np
from sklearn.metrics import r2_score, mean_absolute_error, jaccard_score
import json
import matplotlib.pyplot as plt

# Main function to predict change points and label activities for timeseries data
def main(root_folder):
    columns = ['gyr_x', 'gyr_y', 'gyr_z','acc_x', 'acc_y', 'acc_z']
    
    # Create empty DataFrames to store data for each user
    df_user0 = pd.DataFrame(columns=columns)
    df_user1 = pd.DataFrame(columns=columns)

    timeseries_path = os.path.join(root_folder, 'timeseries')
    output_folder = os.path.join(root_folder, 'timeseries_preds')
    for file in os.listdir(timeseries_path):
        ts_path = os.path.join(timeseries_path, file)
        
        # Load data from the timeseries file
        with open(ts_path, "r") as f:
            json_data = json.load(f)
        
        for user, user_data in json_data.items():
            if user == 'user_0':
                df_user0 = pd.concat([df_user0, pd.DataFrame([user_data])], axis=0, ignore_index=True)
            else:
                df_user1 = pd.concat([df_user1, pd.DataFrame([user_data])], axis=0, ignore_index=True)

    # Extract sensor signal for each user
    signal_0 = df_user0[columns].values
    signal_1 = df_user1[columns].values
    signals = [(0,signal_0), (1, signal_1)]

    # Ground truth values
    ground_truth_0 = np.array([25  , 66  ,112 , 133])
    ground_truth_1 = np.array([25  ,133])
    ground_truth = [ground_truth_0, ground_truth_1]

    # Define activity labels for each change point and user
    user_0_activites = ['Handshacking','Walking', 'TypingSmartphone', 'Walking']
    user_1_activites = ['Handshacking','Walking']
    user_activities = [user_0_activites, user_1_activites]
    
    results = []
    for index, signal in signals:
        # Create and fit the model for change point detection
        algo = rpt.Dynp(model="linear", min_size=22).fit(signal)

        # Choose the number of expected change points
        n_bkps = len(ground_truth[index]) - 1
        
        # Predict change points
        result = algo.predict(n_bkps=n_bkps)
        rpt.display(signal, result)
        plt.xlabel = user_activities[index]
        plt.ylabel = 'martin'
        plt.savefig(os.path.join(root_folder, f'ruptures_pred_vis_{index}.png'))
        results.append(result)


    # Predictions
    predictions_0 = np.array(results[0])
    predictions_1 = np.array(results[1])

    # Calculate R^2
    r_squared_0 = r2_score(ground_truth_0, predictions_0)
    r_squared_1 = r2_score(ground_truth_1, predictions_1)
    # Calculate MAE
    mae_0 = mean_absolute_error(ground_truth_0, predictions_0)
    mae_1 = mean_absolute_error(ground_truth_1, predictions_1)

    # Calculate IoU
    iou_gt_0 = []
    iou_gt_1 = []
    iou_pred_0 = []
    iou_pred_1 = []

    j = 0
    for change_point in ground_truth_0:
        lower_range = len(iou_gt_0)
        for i in range(lower_range, change_point):
            iou_gt_0.append(j)
        j += 1

        
    j = 0
    for change_point in ground_truth_1:
        lower_range = len(iou_gt_1)
        for i in range(lower_range,change_point):
            iou_gt_1.append(j)
        j += 1
        
    j = 0
    for change_point in predictions_0:
        lower_range = len(iou_pred_0)
        for i in range(lower_range,change_point):
            iou_pred_0.append(j)
        j += 1
        
    j = 0
    for change_point in predictions_1:
        lower_range = len(iou_pred_1)
        for i in range(lower_range,change_point):
            iou_pred_1.append(j)
        j += 1
        

    iou_0 = jaccard_score(iou_gt_0, iou_pred_0, average='macro')
    iou_1 = jaccard_score(iou_gt_1, iou_pred_1, average='macro')

    # Create a dictionary to store the results
    evaluation_results = {
        "user_0_activites": user_0_activites,
        "user_1_activites": user_1_activites,
        "r_squared_0": r_squared_0,
        "r_squared_1": r_squared_1,
        "mae_0": mae_0,
        "mae_1": mae_1,
        "iou_0": iou_0,
        "iou_1": iou_1
    }

    # Write the results to a JSON file
    eval_path = os.path.join(root_folder, 'evaluation_results.json')
    with open(eval_path, 'w') as json_file:
        json.dump(evaluation_results, json_file, indent=4)

    print("Evaluation results have been written to evaluation_results.json")



    # Create output folder if it doesn't exist
    os.makedirs(output_folder, exist_ok=True)

    # Initialize index for iterating over files
    i = 0
    # Iterate over files in the timeseries folder again to label activities
    for file in os.listdir(timeseries_path):
        ts = os.path.join(timeseries_path, file)
        timeseries_labeled_path = os.path.join(output_folder, file)
        
        # Load data from the timeseries file
        with open(ts, "r") as f:
            json_data = json.load(f)
        
        # Label activities based on detected change points
        for user, user_data in json_data.items():
            if user == 'user_0':
                if i in results[0]:
                    user_data['activity'] = user_0_activites.pop(0)
                else:
                    user_data['activity'] = user_0_activites[0]
            else:
                if i in results[1]:
                    user_data['activity'] = user_1_activites.pop(0)
                else:
                    user_data['activity'] = user_1_activites[0]
        
        # Write labeled data to output file
        with open(timeseries_labeled_path, 'w') as json_file:
            json.dump(json_data, json_file, indent=4)
        
        i += 1

# Command-line interface for the script
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Predict Ruptures change point detection based activity labels for timeseries.")
    
    parser.add_argument("root_folder", help="Path to root folder contatining timeseries folder (timeseries) & creating output folder (timeseries_preds)")

    args = parser.parse_args()
    
    main(args.root_folder)
