import os
import numpy as np
import open3d as o3d
import argparse
import json
import open3d.visualization.gui as gui
import time

class Viewer3D(object):

    def __init__(self, title):
        self.app = o3d.visualization.gui.Application.instance
        self.app.initialize()

        self.main_vis = o3d.visualization.O3DVisualizer(title, 1280, 980)
        self.main_vis.show_skybox(False)
        self.app.add_window(self.main_vis)

        self.setup_point_clouds()
        self.setup_o3d_scene()

    def setup_point_clouds(self):
        # setup an empty point cloud to be later populated with live data
        self.point_cloud_o3d = o3d.geometry.PointCloud()
        # the name is necessary to remove from the scene
        self.point_cloud_o3d_name = "point cloud"
        self.bbox_names_prev = []
        self.bbox_names_next = []
        self.bbox = []
        self.user_0_label = ''
        self.user_1_label = ''

    def update_data(self, pcd_path, ann_path, timeseries_path):
        # update your point cloud data here: convert depth to point cloud / filter / etc.
        self.point_cloud_o3d = o3d.io.read_point_cloud(pcd_path)
        self.point_cloud_o3d.paint_uniform_color([0.2,0.2,0.2])
        self.bbox, self.bbox_names_next = update_bbox(ann_path)

        with open(timeseries_path, "r") as file:
            json_data = json.load(file)

        for user, user_data in json_data.items():
            print(f"User: {user}")
            print(f"Gyroscope: ({user_data['gyr_x']}, {user_data['gyr_y']}, {user_data['gyr_z']})")
            print(f"Accelerometer: ({user_data['acc_x']}, {user_data['acc_y']}, {user_data['acc_z']})")
            print(f"Heart rate: {user_data['heartrate']}")
            if user == 'user_0' and 'activity' in user_data:
                self.user_0_label = user_data['activity']
            elif 'activity' in user_data:
                self.user_1_label = user_data['activity']
                

    def setup_o3d_scene(self):
        self.main_vis.add_geometry(self.point_cloud_o3d_name, self.point_cloud_o3d)
        self.main_vis.reset_camera_to_default()
        bounds = self.point_cloud_o3d.get_axis_aligned_bounding_box()
        self.main_vis.setup_camera(60, bounds.get_center(),
                                       bounds.get_center() + [0, -30, 10],
                                       [0, 1, 0])

    def update_o3d_scene(self):
        self.main_vis.remove_geometry(self.point_cloud_o3d_name)
        self.main_vis.clear_3d_labels()
        self.main_vis.add_geometry(self.point_cloud_o3d_name, self.point_cloud_o3d)

        for name in self.bbox_names_prev:
            self.main_vis.remove_geometry(f'box_{name}')

        self.bbox_names_prev = self.bbox_names_next[:]
        for box in self.bbox:
            self.main_vis.add_geometry(f'box_{self.bbox_names_next[0]}', box[1])
            
            label_center = box[1].center
            if box[0] == 'user_0':
                self.main_vis.add_3d_label(label_center, f'{box[0]} | {self.user_0_label}')
            elif box[0] == 'user_1':
                self.main_vis.add_3d_label(label_center, f'{box[0]} | {self.user_1_label}')
            else:
                self.main_vis.add_3d_label(label_center, box[0])



            self.bbox_names_next.pop(0)

    def run_one_tick(self):

        app = o3d.visualization.gui.Application.instance
        tick_return = app.run_one_tick()
        if tick_return:
            self.main_vis.post_redraw()
        return tick_return
    

def update_timeseries(timeseries_path):
    # Open the file and load the JSON data
    with open(timeseries_path, "r") as file:
        json_data = json.load(file)
    return json_data

def update_bbox(ann_path):

    # Open the file and load the JSON data
    with open(ann_path, "r") as file:
        json_data = json.load(file)

    boxes = []
    boxes_names = []
    # Extract information for each bbox
    for item in json_data:
        objects = item['objects']
        for obj in objects:
            bbox_id = obj['id']
            center = obj['contour']['center3D']
            rotation = obj['contour']['rotation3D']
            extent = obj['contour']['size3D']
            className = obj['className']
            
            center = np.array([center['x'],center['y'],center['z']], dtype=np.float64)
            extent = np.array([extent['x'],extent['y'],extent['z']], dtype=np.float64)
            R = createRotationMatrix(rotation)
            box = o3d.geometry.OrientedBoundingBox(center, R, extent)
            color = class_to_color[className]
            box.color = color
            boxes.append((className, box))
            boxes_names.append(bbox_id)
    return boxes, boxes_names

def createRotationMatrix(eu_dic):
    # Extract Euler angles
    rx, ry, rz = eu_dic["x"], eu_dic["y"], eu_dic["z"]
    
    # Rotation matrices around x, y, and z axes
    Rx = np.array([[1, 0, 0],
                [0, np.cos(rx), -np.sin(rx)],
                [0, np.sin(rx), np.cos(rx)]])
    
    Ry = np.array([[np.cos(ry), 0, np.sin(ry)],
                [0, 1, 0],
                [-np.sin(ry), 0, np.cos(ry)]])
    
    Rz = np.array([[np.cos(rz), -np.sin(rz), 0],
                [np.sin(rz), np.cos(rz), 0],
                [0, 0, 1]])
    
    # Combined rotation matrix
    R = np.dot(np.dot(Rz, Ry), Rx)
    return R

class_to_color = {
'user_0': [1.0, 0, 0],  # Red color for class 1
'user_1': [0, 1.0, 0],  # Green color for class 2
'Pedestrian': [0, 0, 1.0],  # Blue color for class 3
'Cyclist': [0.8,0.2,0.8],
'Car': [1.0, 0.7, 0.2],  # Blue color for class 3
'Truck': [0, 1.0, 0.9]  # Blue color for class 3
}

def main(root_path):
    # Initial setup & frame range
    pcd_folder = os.path.join(root_path, 'point_cloud')
    annotations_folder = os.path.join(root_path, 'annotations')
    timeseries_folder = os.path.join(root_path, 'timeseries_preds')

    pcd_files = [f for f in os.listdir(pcd_folder)]
    annotations_files = [f for f in os.listdir(annotations_folder)]
    timeseries_files = [f for f in os.listdir(timeseries_folder)]
    
    # Sort the filenames
    asc_filenames = sorted(timeseries_files)

    # Access the JSON with the lowest number
    first_frame = asc_filenames[0].split('.')[0]

    last_frame = asc_filenames[-1].split('.')[0]

    current_frame = first_frame

    viewer3d = Viewer3D("point cloud gui")

    try:
        for i in range (0,len(asc_filenames)):
            time.sleep(0.1)

            current_frame = asc_filenames[i].split('.')[0]
            ann_file = filter(lambda x: x.endswith(f'{current_frame}.json'), annotations_files)
            ann_path = os.path.join(annotations_folder, list(ann_file)[0])

            pcd_file = filter(lambda x: x.endswith(f'{current_frame}.pcd'), pcd_files)
            pcd_path = os.path.join(pcd_folder, list(pcd_file)[0])

            timeseries_file = filter(lambda x: x.endswith(f'{current_frame}.json'), timeseries_files)
            timeseries_path = os.path.join(timeseries_folder, list(timeseries_file)[0])

            viewer3d.update_data(pcd_path,ann_path, timeseries_path)
            # Step 2) Update the cloud and tick the GUI application
            viewer3d.update_o3d_scene()
            if current_frame == last_frame:
                viewer3d.app.quit()
            viewer3d.run_one_tick()
    except Exception as e:
        print(e)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot point cloud with bounding boxes and timeseries data.")
    # Add command-line arguments
    parser.add_argument("root_path", help="Path to the folder containing PCD folder (point_cloud), timeseries folder (timeseries_preds) & annotation folder (annotations)")
    args = parser.parse_args()

    main(args.root_path) 