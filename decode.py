# Importing necessary libraries
import argparse
import yaml   
import sys

sys.path.append("veloparser")

from lidar_manager import *

# Function to read parameters from a YAML configuration file
def read_params(path):
    try:
        f = open(path, 'rb')  
    except Exception as ex:
        print(str(ex)) 

    params = yaml.safe_load(f.read())  
    return params 

# Main function to handle command-line arguments and execute processing
def main(args):
    path = args['path']         # Retrieving the path to the pcap file from command-line arguments
    outdir = args['out_dir']    # Retrieving the output directory path from command-line arguments
    config = args['config']     # Retrieving the configuration file path from command-line arguments

    params = read_params(config)
    print(params) 

    # Instantiating a VelodyneManager object with the provided parameters
    lidar_manager = VelodyneManager(path, outdir, params)

    lidar_manager.run()  

# Entry point of the script
if __name__ == "__main__":
    parser = argparse.ArgumentParser() 

    # Adding command-line arguments for path to pcap file, output directory, and configuration file
    parser.add_argument('-p', '--path', help="Path to the pcap file", required=True)
    parser.add_argument('-o', '--out-dir', help="Path to the output directory", required=True)
    parser.add_argument('-c', '--config', help="Path of the configuration file", required=True)

    args = vars(parser.parse_args()) 
    main(args)  
