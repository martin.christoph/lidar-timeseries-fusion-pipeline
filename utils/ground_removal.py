import argparse
import os
import numpy as np
import open3d as o3d

def visualize_ransac_ground_removal(pcd_folder, frame):
    file_path = os.path.join(pcd_folder, frame)

    pcd = o3d.io.read_point_cloud(file_path)

    # Segment the plane
    plane_model, inliers = pcd.segment_plane(distance_threshold=0.1,
                                                         ransac_n=10,
                                                         num_iterations=1000)
    
    [a, b, c, d] = plane_model
    print(f"Plane equation: {a:.2f}x + {b:.2f}y + {c:.2f}z + {d:.2f} = 0")

    # Find points below the plane
    points = np.array(pcd.points)
    plane_eq = np.dot(points, plane_model[:3]) + plane_model[3]
    below_plane = np.where(plane_eq < 0)[0]

    # Create inlier and outlier clouds
    inliers_cloud = pcd.select_by_index(inliers)
    inliers_cloud.paint_uniform_color([1.0, 0, 0])  # Red color
    below_plane_cloud = pcd.select_by_index(below_plane)
    below_plane_cloud.paint_uniform_color([1.0,1.0,1.0])
    outlier_cloud = pcd.select_by_index(inliers, invert=True)
    outlier_cloud.paint_uniform_color([0, 0, 1.0])  # Blue color
    # Visualize
    o3d.visualization.draw_geometries([outlier_cloud, inliers_cloud])
    print("Visualization complete.")

def ransac_ground_removal(pcd_folder, frame, output_folder):

    os.makedirs(output_folder, exist_ok=True)

    file_path = os.path.join(pcd_folder, frame)
    
    # Create an Open3D PointCloud object
    pcd = o3d.io.read_point_cloud(file_path)
    points = np.array(pcd.points)

    # Segment the plane
    plane_model, inliers = pcd.segment_plane(distance_threshold=0.3,
                                                         ransac_n=3,
                                                         num_iterations=2000)
    
    [a, b, c, d] = plane_model

    # Find points below the plane
    plane_eq = points.dot(plane_model[:3]) + d
    below_plane = np.where(plane_eq < 0)[0]

    # Combine inliers and below plane points
    combined_inliers = np.union1d(inliers, below_plane)

    outlier_cloud = pcd.select_by_index(combined_inliers, invert=True)

    output_path = os.path.join(output_folder, frame)
    
    o3d.io.write_point_cloud(output_path, outlier_cloud)
    print(f"Ground removed from '{file_path}' and saved to '{output_path}'.")

def main(pcd_folder, output_folder, vis):
    files = [f for f in os.listdir(pcd_folder) if f.endswith(".pcd")]
    if not vis:
        print("Starting ground removal process...")
        for file in files:
            ransac_ground_removal(pcd_folder, file, output_folder)
        print("Ground removal process completed.")
    else:
        print("Visualizing RANSAC ground removal with one .pcd file...")
        visualize_ransac_ground_removal(pcd_folder, files[0])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="RANSAC ground removal for specified pcd folder")
    parser.add_argument("pcd_folder", help="Path to pcd folder")
    parser.add_argument("output_folder", help="Path to output folder")
    parser.add_argument("--vis", action='store_true', help="Visualize RANSAC with one .pcd file")
    args = parser.parse_args()
    main(**vars(args))
