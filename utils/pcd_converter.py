import os
import numpy as np
import open3d as o3d
import argparse

def pcd_to_bin(pcd_file, output_dir):
    # Read the PCD file
    pcd = o3d.io.read_point_cloud(pcd_file)
    points = np.asarray(pcd.points)
    # Create intensity array with zeros
    intensity = np.zeros((points.shape[0], 1))

    # Combine points and intensity
    data = np.concatenate((points, intensity), axis=1)
    # Save the points as binary .bin file
    bin_path = os.path.join(output_dir, os.path.basename(pcd_file).replace('.pcd', '.bin'))
    data.astype(np.float32).tofile(bin_path)

def pcd_to_npy(pcd_file, output_dir):
    pcd = o3d.io.read_point_cloud(pcd_file)
    points = np.asarray(pcd.points)

    # Transform the coordinates
    # Create intensity array with zeros
    intensity = np.zeros((points.shape[0], 1))

    # Combine points and intensity
    data = np.concatenate((points, intensity), axis=1)

    # Save as .npy
    npy_file = os.path.join(output_dir, os.path.basename(pcd_file).replace('.pcd', '.npy'))
    np.save(npy_file, data)

def convert_pcd_dir(input_dir, output_dir, npc):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    pcd_files = [f for f in os.listdir(input_dir) if f.endswith('.pcd')]

    for pcd_file in pcd_files:
        pcd_path = os.path.join(input_dir, pcd_file)
        if not npc:
            pcd_to_bin(pcd_path, output_dir)
        else:
            pcd_to_npy(pcd_path, output_dir)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert a directory of .pcd files to .npy format with normalized intensity values.")
    parser.add_argument("input_dir", help="Directory containing input .pcd files")
    parser.add_argument("output_dir", help="Directory to save output .npy files")
    parser.add_argument("--npc", action='store_true', help="Convert to .npy (default convert to .bin)")
    args = parser.parse_args()

    convert_pcd_dir(**vars(args))
