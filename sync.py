import pandas as pd
import numpy as np
import os
import argparse
import json

def main(pcd_folder_path, out_folder_path, session_num):
    print("Reading CSV files...")
    column_names = ['id', 'session', 'timestamp', 'gyr_x', 'gyr_y', 'gyr_z', 'acc_x', 'acc_y', 'acc_z', 'heartrate', 'longitude', 'latitude']

    #file_path = '../../recording_sessions/292n_timeseries.csv'
    file_path = '../../recording_sessions/292n_timeseries.csv'

    print("Reading:", file_path)
    data_292n = pd.read_csv(file_path, header=0, names=column_names)

    #file_path = '../../recording_sessions/28xn_timeseries.csv'
    file_path = '../../recording_sessions/28xn_timeseries.csv'

    print("Reading:", file_path)
    data_28xn = pd.read_csv(file_path, header=0, names=column_names)

    data_28xn = data_28xn[data_28xn['session'] == int(session_num)]
    data_292n = data_292n[data_292n['session'] == int(session_num)]
    data_28xn['user'] = 'user_0'
    data_292n['user'] = 'user_1'

    min_timestamp = max(data_28xn['timestamp'].min(),data_292n['timestamp'].min())
    max_timestamp = min(data_28xn['timestamp'].max(),data_292n['timestamp'].max())
    print(min_timestamp, max_timestamp)
    dfs = [(0, data_28xn), (1, data_292n)]

    print('Creating output directory...')
    os.makedirs(out_folder_path, exist_ok=True)

    print("Processing PCD files...")
    for filename in os.listdir(pcd_folder_path):
        if filename.endswith(".pcd"):
            # Get the timestamp of the pcd frame
            pcd_timestamp = float(filename.split('_')[0])
            frame_number = filename[-10:-4]
            if min_timestamp <= pcd_timestamp <= max_timestamp:
                # Dictionary to store user data
                user_data_dict = {}              
                for index, df in dfs:
                    


                    timestamp_series = df['timestamp']
                    # Find the closest timestamp in your timeseries data
                    closest_timestamp = min(timestamp_series, key=lambda x: abs(x - pcd_timestamp))
                    
                    # Find the index corresponding to the closest timestamp
                    closest_index = timestamp_series.index[timestamp_series == closest_timestamp][0]

                    # Get the timeseries data for the closest timestamp
                    closest_timeseries_data = df.loc[closest_index]

                    if index == 0:
                        # Add user_1 data to dictionary
                        user_1_dict = closest_timeseries_data[['gyr_x', 'gyr_y', 'gyr_z', 'acc_x', 'acc_y', 'acc_z', 'heartrate', 'longitude', 'latitude']].to_dict()
                        user_data_dict['user_0'] = user_1_dict
                    else:
                        # Add user_2 data to dictionary
                        user_2_dict = closest_timeseries_data[['gyr_x', 'gyr_y', 'gyr_z', 'acc_x', 'acc_y', 'acc_z', 'heartrate', 'longitude', 'latitude']].to_dict()
                        user_data_dict['user_1'] = user_2_dict
                

                # Create a JSON file and write the dictionary to it
                json_filename = f"{frame_number}.json"
                json_folder = os.path.join(out_folder_path, json_filename)
                with open(json_folder, 'a+') as json_file:
                    json.dump(user_data_dict, json_file, indent=4)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create timeseries entries to syncronize it with pcd data.")
    
    # Add command-line arguments
    parser.add_argument("pcd_folder_path", help="Path to the folder containing PCD files")
    parser.add_argument("out_folder_path", help="Path to the output folder")
    parser.add_argument("session", help="Define sessions to synchronize")

    args = parser.parse_args()

    main(args.pcd_folder_path, args.out_folder_path, args.session)
